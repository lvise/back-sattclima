require('dotenv').config()
const express = require('express')
const cors = require('cors')
const app = express()
const nodemailer = require('nodemailer')

app.use(cors())
// app.use(express.urlencoded({ extended: false }))
app.use(express.json())

// app.all("/*", function (req, res, next) {

//     res.header('Access-Control-Allow-Origin', '*');
//     res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
//     res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, X-Requested-With');
//     next();
// });

app.get('/', (req, res) => {
    res.send('test123')
})

app.post('/email', (req, res) => {
    console.log('req', req.body)
    let transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        service: 'gmail',
        port: 587,
        secure: true,
        auth: {
            user: process.env.MAIL_USER,
            pass: process.env.MAIL_PWD
        }
    })

    let mailContent = {
        from: req.body.from,
        to: req.body.to,
        subject: req.body.subject,
        text: req.body.text
    }

    transporter.sendMail(mailContent, (err) => {
        if (err) {
            res.send(`ERRO ${err}`)
            res.sendStatus(500)
        }
        else {
            res.send('Email enviado com sucesso')
            res.sendStatus(200)
        }
    })
})

app.listen(3000)

